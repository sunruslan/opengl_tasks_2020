#include "./common/Mesh.hpp"
#include <glm/simd/geometric.h>
#include <vector>
#include <algorithm>


glm::vec3 transform_to_xyz(float R, float r, float theta, float phi) {
    return glm::vec3((R + r * cos(phi)) * cos(theta), (R + r * cos(phi)) * sin(theta), r * sin(phi));
}

glm::vec3 normal(float R, float r, float theta, float phi) {
    glm::vec3 end = transform_to_xyz(R, r, theta, phi);
    glm::vec3 beg = glm::vec3(R * cos(theta), R * sin(theta), 0);
    glm::vec3 normal = glm::vec3(beg.x - end.x, beg.y - end.y, beg.z - end.z);
    return normalize(normal);
}

MeshPtr generateSurface(float R, float r, unsigned int N) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (size_t i = 0; i < N; i++) {
        float theta = 2 * glm::pi<float>() * i / N;
        float theta1 = 2 * glm::pi<float>() * (i + 1) / N;
        for (size_t j = 0; j < N; j++) {
            float phi = 2 * glm::pi<float>() * j / N;
            float phi1 = 2 * glm::pi<float>() * (j + 1) / N;
            vertices.emplace_back(transform_to_xyz(R, r, phi, theta));
            vertices.emplace_back(transform_to_xyz(R, r, phi1, theta1));
            vertices.emplace_back(transform_to_xyz(R, r, phi, theta1));

            normals.emplace_back(normal(R, r, phi, theta));
            normals.emplace_back(normal(R, r, phi1, theta1));
            normals.emplace_back(normal(R, r, phi, theta1));

            vertices.emplace_back(transform_to_xyz(R, r, phi, theta));
            vertices.emplace_back(transform_to_xyz(R, r, phi1, theta1));
            vertices.emplace_back(transform_to_xyz(R, r, phi1, theta));

            normals.emplace_back(normal(R, r, phi, theta));
            normals.emplace_back(normal(R, r, phi1, theta1));
            normals.emplace_back(normal(R, r, phi1, theta));

        }
    }
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());
    return mesh;
}