#version 330

uniform sampler2D diffuseTex;

in vec2 texCoord; 
in vec3 normalCamSpace; 
in vec4 posCamSpace; 

out vec4 fragColor;

struct LightInfo
{
	vec3 pos; 
	vec3 La;
	vec3 Ld;
	vec3 Ls;
};
uniform LightInfo light;


const vec3 Ks = vec3(1.0, 1.0, 1.0);
const float shininess = 128.0;


void main()
{
  	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;

  	vec3 normal = normalize(normalCamSpace); 
  	vec3 viewDirection = normalize(-posCamSpace.xyz);

  	vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz);

  	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0);

  	vec3 color = diffuseColor * (light.La + light.Ld * NdotL);

  	if (NdotL > 0.0)
  	{
  		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection);

  		float blinnTerm = max(dot(normal, halfVector), 0.0);
  		blinnTerm = pow(blinnTerm, shininess);
  		color += light.Ls * Ks * blinnTerm;
  	}

  	fragColor = vec4(color, 1.0);
}